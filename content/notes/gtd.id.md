---
title: "Gtd"
date: 2023-09-06T11:51:28+07:00
tags: ['emacs', 'planner', 'gtd']
categories: ['notes']
draft: true
cover:
  image: "/images/GTDcanonical.png"
  alt: "Getting Things Done"
  caption: "From wikipedia.org"
  relative: false # To use relative path for cover image, used in hugo Page-bundles
---

## Getting Things Done
*Getting Things Done* ([GTD](https://en.wikipedia.org/wiki/Getting_Things_Done)) adalah sistem produktifitas pribadi yang dikembangkan oleh _David Alled_ dan diterbitkan dalam bentuk buku dengan nama yang sama. _GTD_ dapat digambarkan sebagai sistem manajemen waktu.

Sistem didalam _GTD_ membutuhkan seseorang untuk memiliki peralatan berikut dalam jangkauan mudah:

-   Inbox
-   Trash can / tempat pembuangan
-   Sistem pengarsipan untuk material referensi
-   beberapa bentuk daftar - daftar
-   kalender

Perlengkapan tersebut bisa dalam bentuk fisik atau elektronik sesuai kebutuhan. Lalu, seiring dengan masuknya "informasi - informasi", ditangkap dalam perlengkapan ini dan diproses dengan alur kerja berikut.

### Alur

Alur kerja _GTD_ terdiri dari lima tahap. Alur kerja tersebut didorong oleh lima pertanyaan: _capture_, _clarify_, _organize_, _reflect_, dan _engage_. Begitu semua "material" ditangkap dalam _inbox_, setiap "barang" di "klarifikasi" dan diorganisir dengan menanyakan dan menjawab pertanyaan - pertanyaan mengenai barang tersebut seperti terlihat didalam kotak hitam dalam diagram. Hasilnya adalah setiap barang akan berada disalah satu dari delapan titik akhir (yang berbentuk oval) didalam diagram:

-   Ditempat pembuangan
-   Dalam daftar "nanti"
-   Dalam sistem pengarsipan yang rapih
-   Dalam daftar tugas - tugas, jika hasil akhir dan aksi berikutnya ditentukan jika tugas yang belum selesai adalah sebuah "project" (misalkan hal ini membutuhkan beberapa langkah untuk dapat diselesaikan)
-   Dapat diselesaikan secepatnya dalam waktu kurang dari 2 menit
-   Didelegasikan ke orang lain dan, jika diperlukan, ditambahkan ke daftar "waiting for"
-   Dalam daftar "next action" yang berbasis konteks jika hanya memerlukan satu langkah untuk diselesaikan

## GTD dalam Emacs

Konfigurasi minimum untuk emacs. Kode - kode ini berada didalam `~/.config/emacs/init.el`

```emacs-lisp
(require 'org)
(add-to-list 'org-modules 'org-habit t)
(setq org-directory (expand-file-name "Sync/org" (getenv "HOME")))
(setq org-todo-keywords
      '((sequence
         "TODO(t!)"  ; A task that needs doing & is ready to do
         "NEXT(n!)"  ; Tasks that can be delayed
         "PROJ(p!)"  ; A project, which usually contains other tasks
         "PROG(g!)"  ; A task that is in progress
         "WAIT(w!)"  ; Something external is holding up this task
         "HOLD(h!)"  ; This task is paused/on hold because of me
         "IDEA(i!)"  ; An unconfirmed and unapproved task or notion
         "|"
         "DONE(d!)"  ; Task successfully completed
         "DELEGATED(l!)" ; Task is delegated
         "KILL(k!)") ; Task was cancelled, aborted or is no longer applicable
        ))
(global-set-key (kbd "C-c l") #'org-store-link)
(global-set-key (kbd "C-c a") #'org-agenda)
(global-set-key (kbd "C-c c") #'org-capture)
(global-set-key (kbd "C-c C-s") #'org-schedule)
```

*Todo-keywords* di *org-mode* membuat setiap judul yang diawali dengan _keyword - keyword_ ini menjadi _*TODO* item_. Disini kita mengatur daftar kustom _keyword - keyword_. Setiap _item_ sebelum tanda `|` adalah _*TODO* keyword_ (membutuhkan aksi), dan setelahnya adalah status *DONE*, yang tidak membutuhkan aksi.

```emacs-lisp
(setq org-todo-keyword-faces
   '(("PROJ" . (:foreground "cyan" :weight bold))
     ("WAIT" . (:foreground "yellow" :weight bold))
     ("DELEGATED" . "green")
     ("KILL" . "green")))
```

_Function_ `org-capture` berfungsi untuk menangkap setiap _item_ di _Emacs_, `org-agenda` adalah tampilan agenda-nya, dan `org-store-link` berfungsi untuk menyimpan _link_(URL, _path_ file, gambar, dan lainnya). Variabel `org-directory` adalah lokasi untuk file - file `org-mode`.

### Capture

![capture](/images/2023-09-06_13-05-org-capture.png)

Perintah `C-c c` akan memberikan pilihan _template - template_ yang akan digunakan untuk _capture_.

```emacs-lisp
(require 'org-capture)
(setq org-capture-templates
      `(("i" "Inbox" entry
         (file+headline ,(expand-file-name "inbox.org" org-directory) "Inbox")
         "** %?\n:PROPERTIES:\n:CREATED: %U\n:END:\n\n%i\n\nFrom: %a" :empty-lines 1 :prepend t)))
```

Kode diatas akan membuat "i" didalam `org-capture-template` untuk menyimpan entri dibawah judul "Inbox" di file `~/Sync/org/todo.org`. Semuanya dimulai dari file ini. `"** %?\n:PROPERTIES:\n:CREATED: %U\n:END:\n\n%i\n\nFrom: %a"` adalah ekspansi _template_ `org-capture`,

-   `**` akan membuat _item_ yang di _capture_ berada di judul level kedua.
-   `%?` adalah posisi kursor setelah _template_ diselesaikan.
-   `%U` akan menaruh _stempel waktu_ dengan penambahan jam.
-   `%i` adalah konten awal.
-   `%a` meruapakan anotasi.
-   `:empty-lines 1` akan membuat baris kosong sebelum dan sesudah _item_.
-   `:prepend t` akan menaruh _item_ di daftar paling atas.


### Clarify + Organize

Berdasarkan alur kerja diatas, kita perlu memisahkan setiap _item_ di tempatnya masing - masing.

-   **`archives.org`:** untuk pembuangan, namun masih dibutuhkan jejak waktunya.
-   **`notes.org`:** untuk menyimpan catatan - catatan referensi, tugas - tugas yang tidak berjadwal, perlu didelegasikan ke orang lain, atau aksi dengan prioritas rendah.
-   **`projects.org`:** untuk tugas - tugas yang membutuhkan beberapa langkah - langkah untuk dapat diselesaikan.
-   **`todo.org`:** untuk tugas - tugas yang dapat dikerjakan segera.
-   **`habits.org`:** untuk tugas - tugas berulang.
-   Aksi atau tugas - tugas yang berdasarkan konteks waktu akan ditandai dengan _tag_.

```emacs-lisp
(setq org-tag-alist '(("work" . ?w)
                      ("followup" . ?f)
                      ("project" . ?p)
                      ("personal" . ?h)
                      ("reading" . ?r)
                      ("links" . ?l)))
```

*Tags* adalah fitur dari *org-mode _Emacs_*, yang memudahkan kita untuk mengklasifikasi informasi.

```emacs-lisp
(setq org-todo-state-tags-triggers
'(("KILL" ("killed" . t) ("Archives" . t))
  ("WAIT" ("wait" . t))
  ("HOLD" ("wait") ("hold" . t))
  (done ("wait") ("hold"))
  ("PROJ" ("project" . t))
  ("TODO" ("wait") ("killed") ("hold"))
  ("NEXT" ("wait") ("killed") ("hold"))
  ("PROG" ("wait") ("killed") ("hold"))
  ("STRT" ("wait") ("killed") ("hold"))
  ("DONE" ("wait") ("killed") ("hold"))))
```
Ini akan menjadi pemicu _tag_ otomatis untuk setiap judul:
- Yang ditandai _keyword_ `TODO` akan otomatis diberi _tag_ `killed` dan `Archives`.
- Yang ditandai _keyword_ `WAIT` akan otomatis diberi _tag_ `wait`.
- Yang ditandai _keyword_ `HOLD` akan otomatis diberi _tag_ `hold`, dan _tag_ `wait` akan dihapus.
- Yang ditandai _keyword_ `DONE` akan menghapus _tag_ `wait` dan `hold`.
- Yang ditandai _keyword_ `PROJ` akan diberi _tag_ `project`.
- Yang ditandai _keyword_ `TODO`, `STRT`, `NEXT`, `PROG`, and `DONE` akan menghapus _tag_ `wait`, `killed`, dan `hold`.

Klarifikasi setiap informasi - informasi baru, pemikiran - pemikiran, _email_, dan lainnya didalam `inbox.org`.
-   Jika _item_ tersebut tidak membutuhkan aksi, namun masih perlu disimpan, taruh didalam `notes.org`.
-   Jika _item_ tersebut dapat diproses, dan membutuhkan waktu kurang dari 2 menit, lakukan segera, jika diperlukan, catat dengan _keyword_ `DONE`.
-   Jika membutuhkan lebih dari satu tahap untuk dapat diselesaikan, tugas tersebut berada didalam _project_, tandai dengan _keyword_ `PROJ`, buat subjudul untuk setiap tahapan tambahannya dan simpan didalam `projects.org`.
-   Jika dapat diproses, namun sebelumnya perlu diproses oleh orang / atau tugas lainnya, tandai dengan _keyword_ `WAIT`, sampai dapat diproses oleh kita.
-   Jika prosesnya hanya dapat dilakukan diwaktu tertentu, tandai dengan _keyword_ `NEXT`.

Variabel `org-todo-keywords` akan menentukan _keyword - keyword_ yang dipakai sebagai *_TODO item_*, yang _default_-nya adalah perputaran dari _keyword_ `TODO` dan `DONE`. Ini merupakan bagian dari alur _`Clarify`_.

Satu file yang dapat membesar dengan cepat adalah `archives.org`, jadi kita perlu membuat tugas _berulang_ di file `habits.org`:

```orgmode
* Habits
** TODO Tasks Cleanup
SCHEDULED: <2023-09-01 Fri +1w>
:PROPERTIES:
:STYLE:    habit
:END:
```

Tugas ini akan berulang mingguan, mengingatkan saya untuk membersihkan file - file `org-mode`.

- Refile

![refile](/images/2023-09-06_16-04-org-refile.png)

```emacs-lisp
(require 'org-refile)
(setq org-refile-targets
      `((,(expand-file-name "archives.org" org-directory) :maxlevel . 1)
        (,(expand-file-name "notes.org" org-directory) :maxlevel . 1)
        (,(expand-file-name "projects.org" org-directory) :maxlevel . 1)
        (,(expand-file-name "todo.org" org-directory) :maxlevel . 1)))
(setq org-refile-use-outline-path 'file
      org-outline-path-complete-in-steps nil)
```

Sebagai bagian dari alur _`Organize`_, `org-refile` digunakan untuk memindahkan informasi per judul, dengan tujuan akhir membersihkan entri - entri didalam `inbox.org` kedalam file - file lain.

### Reflect

Tinjauan umum dari daftar tugas - tugas, prioritas, dan sasaran. Dalam `Emacs` kita menggunakan `org-agenda`.

```emacs-lisp
(require 'org-agenda)
(setq org-clock-in-resume t
      org-clock-out-remove-zero-time-clocks t
      org-clock-history-length 20
      org-show-notification-handler "notify-send"
      org-agenda-skip-scheduled-if-deadline-is-shown t
      org-stuck-projects '("+{project*}-killed-Archives/-DONE-KILL-DELEGATED"
                         ("TODO" "NEXT" "IDEA" "PROG")
                         nil ""))
```
Variabel `org-agenda-custom-commands` akan mendefinisikan isi dari layar utama yang ditampilkan oleh _agenda dispatcher_.

```emacs-lisp
(setq org-agenda-custom-commands
      `(("a" "All Agenda"
         ((agenda ""
                  ((org-agenda-span 1)
                   (org-agenda-block-separator nil)
                   (org-deadline-warning-days 0)
                   (org-agenda-day-face-function (lambda (date) 'org-agenda-date))
                   (org-agenda-start-on-weekday 1)
                   (org-scheduled-past-days 0)
                   (org-agenda-overriding-header "\nToday\n")))
          (agenda ""
                  ((org-agenda-block-separator nil)
                   (org-agenda-start-day "+1d")
                   (org-agenda-overriding-header "\nAll Agendas\n")))))
        ("w" . "Work")
        ("wa" "All Work Agenda"
         ((tags-todo "+followup"
                     ((org-agenda-block-separator nil)
                      (org-agenda-overriding-header "\nNeeds Followup\n")))
          (agenda ""
                  ((org-agenda-span 1)
                   (org-agenda-block-separator nil)
                   (org-deadline-warning-days 0)
                   (org-scheduled-past-days 0)
                   (org-agenda-day-face-function (lambda (date) 'org-agenda-date))
                   (org-agenda-format-date "%A %-e %B %Y")
                   (org-agenda-start-on-weekday 1)
                   (org-agenda-overriding-header "\nToday's Work\n")))
          (agenda ""
                  ((org-agenda-start-on-weekday nil)
                   (org-agenda-start-day "+1d")
                   (org-agenda-span 3)
                   (org-deadline-warning-days 0)
                   (org-agenda-block-separator nil)
                   (org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                   (org-agenda-overriding-header "\nNext three days\n")))
          (agenda ""
                  ((org-agenda-time-grid nil)
                   (org-agenda-start-day "+4d")
                   (org-agenda-span 14)
                   (org-agenda-show-all-dates nil)
                   (org-deadline-warning-days 0)
                   (org-agenda-block-separator nil)
                   (org-agenda-entry-types '(:deadline))
                   (org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                   (org-agenda-overriding-header "\nUpcoming deadlines\n"))))
         ((org-agenda-tag-filter-preset '("+work" "-personal"))))
        ("h" . "Personal")
        ("ha" "Personal Agenda"
        ((tags-todo "+followup"
                   ((org-agenda-block-separator nil)
                    (org-agenda-overriding-header "\nNeeds Followup\n")))
        (agenda ""
                ((org-agenda-block-separator nil)
                 (org-agenda-overriding-header "\nPersonal Agenda\n")))
        (alltodo ""))
       ((org-agenda-tag-filter-preset '("+personal" "-work"))))
        ("p" . "Projects")
        ("pa" "All Projects"
         ((tags-todo "+{project*}+TODO=\"PROJ\""
                     ((org-agenda-block-separator nil)
                      (org-agenda-overriding-header "\nAll Projects\n")))))
        ("pp" "Personal Projects"
         ((tags-todo "+{project*}+personal+TODO=\"PROJ\""
                     ((org-agenda-block-separator nil)
                      (org-agenda-overriding-header "\nPersonal Projects\n")))))
        ("ps" "Stuck Projects"
         ((stuck ""
                 ((org-agenda-block-separator nil)
                  (org-agenda-overriding-header "\nStuck Projects\n")))))))
```

![org-agenda](/images/2023-09-08_11-10-org-agenda-view.png)
### Engage
Minimalkan waktu yang diperlukan untuk mengelola daftar - daftar ini, gunakan aplikasi _mobile_ untuk sinkronisasi file - file di _org-directory_. Saya menggunakan [syncthing](https://syncthing.net/) untuk sinkronisasi file - file tersebut, dan [orgzly](https://github.com/orgzly/orgzly-android) untuk pengelolaan file - file _org-mode_.

Poin utama dari sistem produktifitas ini adalah untuk memfokuskan perhatian kita lebih ke pengambilan aksi / keputusan.
