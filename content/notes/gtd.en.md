---
title: "Gtd"
date: 2023-09-06T11:51:26+07:00
tags: ['emacs', 'planner', 'gtd']
categories: ['notes']
draft: false
cover:
  image: "/images/GTDcanonical.png"
  alt: "Getting Things Done"
  caption: "From wikipedia.org"
  relative: false # To use relative path for cover image, used in hugo Page-bundles
---

## Getting Things Done

*Getting Things Done* ([GTD](https://en.wikipedia.org/wiki/Getting_Things_Done)) is a personal productivity system developed by David Allen and published in a book of the same name. GTD is described as a time management system. Allen states "there is an inverse relationship between things on your mind and those things getting done".

The system in GTD requires one to have the following tools within easy reach:

-   Inbox
-   Trash can
-   A filing system for reference material
-   Several lists
-   A calendar

These tools can be physical or electronic as appropriate (e.g., a physical "in" tray or an email inbox). Then, as "stuff" enters one's life, it is captured in these tools and processed with the following workflow.


### Workflow

The GTD workflow consists of five stages. The workflow is driven by five questions: capture, clarify, organize, reflect, and engage. (The first edition used the names collect, process, organize, plan, and do; the descriptions of the stages are similar in both editions). Once all the material ("stuff") is captured (or collected) in the inbox, each item is clarified and organized by asking and answering questions about each item in turn as shown in the black boxes in the logic tree diagram. As a result, items end up in one of the eight oval end points in the diagram:

-   in the trash
-   on the someday/maybe list
-   in a neat reference filing system
-   on a list of tasks, with the outcome and next action defined if the "incomplete" is a "project" (i.e., if it will require two or more steps to complete it)
-   immediately completed and checked off if it can be completed in under two minutes
-   delegated to someone else and, if one wants a reminder to follow up, added to a "waiting for" list
-   on a context-based "next action" list if there is only one step to complete it

## Getting Things Done with Emacs

The minimum configuration for _Emacs,_ this code goes to `~/.config/emacs/init.el`

```emacs-lisp
(require 'org)
(add-to-list 'org-modules 'org-habit t)
(setq org-directory (expand-file-name "Sync/org" (getenv "HOME")))
(setq org-todo-keywords
      '((sequence
         "TODO(t!)"  ; A task that needs doing & is ready to do
         "NEXT(n!)"  ; Tasks that can be delayed
         "PROJ(p!)"  ; A project, which usually contains other tasks
         "PROG(g!)"  ; A task that is in progress
         "WAIT(w!)"  ; Something external is holding up this task
         "HOLD(h!)"  ; This task is paused/on hold because of me
         "IDEA(i!)"  ; An unconfirmed and unapproved task or notion
         "|"
         "DONE(d!)"  ; Task successfully completed
         "DELEGATED(l!)" ; Task is delegated
         "KILL(k!)") ; Task was cancelled, aborted or is no longer applicable
        ))
(global-set-key (kbd "C-c l") #'org-store-link)
(global-set-key (kbd "C-c a") #'org-agenda)
(global-set-key (kbd "C-c c") #'org-capture)
(global-set-key (kbd "C-c C-s") #'org-schedule)
```

*Org-mode* *todo-keywords* which makes every headlines beginning with the keywords became a *TODO* item. Here we set a custom list of keywords, every item before the `|` is a *TODO* keywords (need action) and after it is the *DONE* state.

```emacs-lisp
(setq org-todo-keyword-faces
   '(("PROJ" . (:foreground "cyan" :weight bold))
     ("WAIT" . (:foreground "yellow" :weight bold))
     ("DELEGATED" . "green")
     ("KILL" . "green")))
```

The function `org-capture` is for capturing items, `org-agenda` is the agenda view, and `org-store-link` is for storing links(URL, file paths, images, etc). The variable `org-directory` is the location for `org-mode` files.

### Capture

![capture](/images/2023-09-06_13-05-org-capture.png)

The command `C-c c` will let us select the templates used for capturing.

```emacs-lisp
(require 'org-capture)
(setq org-capture-templates
      `(("i" "Inbox" entry
         (file+headline ,(expand-file-name "inbox.org" org-directory) "Inbox")
         "** %?\n:PROPERTIES:\n:CREATED: %U\n:END:\n\n%i\n\nFrom: %a" :empty-lines 1 :prepend t)))
```

This will set the key "i" inside the `org-capture-templates` to store entries under the heading "Inbox" in the `~/Sync/org/todo.org` file. Everything began in this file. The `"** %?\n:PROPERTIES:\n:CREATED: %U\n:END:\n\n%i\n\nFrom: %a"` is an `org-capture` template expansion,

-   `**` will set the captured item in the second level heading.
-   `%?` is the position of the cursor after the template is completed.
-   `%U` will insert an inactive timestamp with date and time.
-   `%i` initial content.
-   `%a` annotation.
-   `:empty-lines 1` will create an empty-line before and after the item.
-   `:prepend t` prepend the item in the file.


### Clarify + Organize

Based from the workflow above we need to separate every items within their respective files.

-   **`archives.org`:** for trash, but still need to track its timestamp.
-   **`notes.org`:** for storing notes of reference, unscheduled tasks, needs to be delegated to someone, or low priority actions.
-   **`projects.org`:** for storing tasks that requires multiple steps to finish/complete.
-   **`todo.org`:** for tasks that can be processed immediately.
-   **`habits.org`:** for recurring tasks.
-   Context-based actions will use `org-mode` tags.


```emacs-lisp
(setq org-tag-alist '(("work" . ?w)
                      ("followup" . ?f)
                      ("project" . ?p)
                      ("personal" . ?h)
                      ("reading" . ?r)
                      ("links" . ?l)))
```

*Tags* is a feature of *Emacs* *org-mode*, which makes it easier to classify informations.

```emacs-lisp
(setq org-todo-state-tags-triggers
'(("KILL" ("killed" . t) ("Archives" . t))
  ("WAIT" ("wait" . t))
  ("HOLD" ("wait") ("hold" . t))
  (done ("wait") ("hold"))
  ("PROJ" ("project" . t))
  ("TODO" ("wait") ("killed") ("hold"))
  ("NEXT" ("wait") ("killed") ("hold"))
  ("PROG" ("wait") ("killed") ("hold"))
  ("STRT" ("wait") ("killed") ("hold"))
  ("DONE" ("wait") ("killed") ("hold"))))
```
This will triggers automatic tag creation for each headings:
- Heading marked with `KILL` keyword will automatically tagged `killed` and `Archives`.
- Heading marked with `WAIT` keyword will be tagged with `wait`.
- Heading marked with `HOLD` keyword will be tagged with `hold`, and will remove the `wait` tag.
- Heading marked with `DONE` keyword will remove the `wait` and `hold` tag.
- Heading marked with `PROJ` keyword will be tagged `project` tag.
- Heading marked with `TODO`, `STRT`, `NEXT`, `PROG`, and `DONE` keyword will remove the `wait`, `killed`, and `hold` tag.

Clarify every new informations, thoughts, emails, etc that arrives in your `inbox.org`.
-   If it does not require us to take action, it should go into `notes.org`.
-   If it is actionable, and should only take 2 minutes or so, then just do it, it doesn't have to be recorded, or if necessary, mark it `DONE`.
-   If it takes more than one step to complete, it is a project, mark it with the `PROJ` keyword, create subheadings for each subtasks and store it in `projects.org`.
-   If it is actionable, and needs to be processed by someone else first, it should be marked `WAIT`, until it became ready.
-   If it is actionable at a later time, put it in the `NEXT` keyword.

The variable `org-todo-keywords` will sets up the keywords available for the TODO items, the default is a rotation of `TODO` and `DONE` keywords. This is part of the `Clarify` workflow.

One file that could get big quickly is `archives.org`, so we'll create our first recurring task in the file `habits.org` to create a new tasks.

```orgmode
* Habits
** TODO Tasks Cleanup
SCHEDULED: <2023-09-01 Fri +1w>
:PROPERTIES:
:STYLE:    habit
:END:
```

This will create a recurring tasks weekly, reminding me to cleanup my org files.

- Refile

![refile](/images/2023-09-06_16-04-org-refile.png)

```emacs-lisp
(require 'org-refile)
(setq org-refile-targets
      `((,(expand-file-name "archives.org" org-directory) :maxlevel . 1)
        (,(expand-file-name "notes.org" org-directory) :maxlevel . 1)
        (,(expand-file-name "projects.org" org-directory) :maxlevel . 1)
        (,(expand-file-name "todo.org" org-directory) :maxlevel . 1)))
(setq org-refile-use-outline-path 'file
      org-outline-path-complete-in-steps nil)
```

As for the `Organize` workflow, we'll use `org-refile` for moving information (per heading). The goal is to move the entries in the `inbox.org` file into their own files.


### Reflect

A regular review of your list of actions, priorities, and goals. In `Emacs` we'll use `org-agenda`.

```emacs-lisp
(require 'org-agenda)
(setq org-clock-in-resume t
      org-clock-out-remove-zero-time-clocks t
      org-clock-history-length 20
      org-show-notification-handler "notify-send"
      org-agenda-skip-scheduled-if-deadline-is-shown t
      org-stuck-projects '("+{project*}-killed-Archives/-DONE-KILL-DELEGATED"
                         ("TODO" "NEXT" "IDEA" "PROG")
                         nil ""))
```

The variable `org-agenda-custom-commands` will define the splash screen displayed by the agenda dispatcher.

```emacs-lisp
(setq org-agenda-custom-commands
      `(("a" "All Agenda"
         ((agenda ""
                  ((org-agenda-span 1)
                   (org-agenda-block-separator nil)
                   (org-deadline-warning-days 0)
                   (org-agenda-day-face-function (lambda (date) 'org-agenda-date))
                   (org-agenda-start-on-weekday 1)
                   (org-scheduled-past-days 0)
                   (org-agenda-overriding-header "\nToday\n")))
          (agenda ""
                  ((org-agenda-block-separator nil)
                   (org-agenda-start-day "+1d")
                   (org-agenda-overriding-header "\nAll Agendas\n")))))
        ("w" . "Work")
        ("wa" "All Work Agenda"
         ((tags-todo "+followup"
                     ((org-agenda-block-separator nil)
                      (org-agenda-overriding-header "\nNeeds Followup\n")))
          (agenda ""
                  ((org-agenda-span 1)
                   (org-agenda-block-separator nil)
                   (org-deadline-warning-days 0)
                   (org-scheduled-past-days 0)
                   (org-agenda-day-face-function (lambda (date) 'org-agenda-date))
                   (org-agenda-format-date "%A %-e %B %Y")
                   (org-agenda-start-on-weekday 1)
                   (org-agenda-overriding-header "\nToday's Work\n")))
          (agenda ""
                  ((org-agenda-start-on-weekday nil)
                   (org-agenda-start-day "+1d")
                   (org-agenda-span 3)
                   (org-deadline-warning-days 0)
                   (org-agenda-block-separator nil)
                   (org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                   (org-agenda-overriding-header "\nNext three days\n")))
          (agenda ""
                  ((org-agenda-time-grid nil)
                   (org-agenda-start-day "+4d")
                   (org-agenda-span 14)
                   (org-agenda-show-all-dates nil)
                   (org-deadline-warning-days 0)
                   (org-agenda-block-separator nil)
                   (org-agenda-entry-types '(:deadline))
                   (org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                   (org-agenda-overriding-header "\nUpcoming deadlines\n"))))
         ((org-agenda-tag-filter-preset '("+work" "-personal"))))
        ("h" . "Personal")
        ("ha" "Personal Agenda"
        ((tags-todo "+followup"
                   ((org-agenda-block-separator nil)
                    (org-agenda-overriding-header "\nNeeds Followup\n")))
        (agenda ""
                ((org-agenda-block-separator nil)
                 (org-agenda-overriding-header "\nPersonal Agenda\n")))
        (alltodo ""))
       ((org-agenda-tag-filter-preset '("+personal" "-work"))))
        ("p" . "Projects")
        ("pa" "All Projects"
         ((tags-todo "+{project*}+TODO=\"PROJ\""
                     ((org-agenda-block-separator nil)
                      (org-agenda-overriding-header "\nAll Projects\n")))))
        ("pp" "Personal Projects"
         ((tags-todo "+{project*}+personal+TODO=\"PROJ\""
                     ((org-agenda-block-separator nil)
                      (org-agenda-overriding-header "\nPersonal Projects\n")))))
        ("ps" "Stuck Projects"
         ((stuck ""
                 ((org-agenda-block-separator nil)
                  (org-agenda-overriding-header "\nStuck Projects\n")))))))
```

![org-agenda](/images/2023-09-08_11-10-org-agenda-view.png)
### Engage

Keep the time needed to manage this list at minimum, use mobile apps for synchronizing the _org-directory_ from and to your mobile devices. I use [syncthing](https://syncthing.net/) for continuous file synchronization and [orgzly](https://github.com/orgzly/orgzly-android) for quickly modify my _org-mode_ files from my android, I only need to point it to the synchronized directory to make it auto update the files.

The main point of this personal productivity system is to allow us to focus our attention on taking action, instead of recalling them.
