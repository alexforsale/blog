---
title: "Tentang Penulis"
date: 2023-08-25T10:06:38+07:00
layout: pages
cover:
  image: "/images/alexforsale2.jpg"
  alt: "Me"
  caption: "Me"
  relative: false # To use relative path for cover image, used in hugo Page-bundles
---

Saya menggunakan _Linux_ lebih dari satu dekade. Dan saya menggunakannya sebagai _operating system_ utama, bukan sebagai _distro_ test di partisi tambahan dengan _dual-booting_. Saya tertarik dengan keterbukaan, kemudahan akses, dan kebebasan dari limitasi yang biasanya terasa di _operating system legacy_.

Tentunya ada sisi lain dari semua itu; dulu ketika saya baru mencoba _Linux_, kompatibilitas _hardware_ sangatlah terbatas, untuk dapat booting dengan tampilan _X11_ kemungkinannya kecil, dan _internet_ tidak seperti _internet_ saat ini.
