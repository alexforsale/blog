---
title: "About Me"
date: 2023-08-25T10:06:38+07:00
layout: pages
cover:
  image: "/images/alexforsale2.jpg"
  alt: "Me"
  caption: "Me"
  relative: false # To use relative path for cover image, used in hugo Page-bundles
---

# Intro into Linux

I've used _Linux_ for over a decade now. Using as a daily driver, not just a test partition in a dual booted machines. I was mesmerized by its openness, accessibiliy, and free from limitations.

Of course there's another side of this; back then hardware compatibility was scarce, X11 was a wild beast and internet surely didn't look like what it is today.

Funnily enough, I never had an occupation that relates to _Linux_ (until now that is). It stays as a hobby, something to tinker around at home.

## Days of the Debian Derivatives

It's all started from _Debian_, just like everybody else. I started to mess around with packages and packages repositories, something that didn't exists in my experience with previous operating systems.

![ubuntu software center](https://3.bp.blogspot.com/_JSR8IC77Ub4/TSS-1jor9UI/AAAAAAAABmI/lKeaOC3Zzjk/s1600/800px-UbuntuMaverickDesktop.png "image courtesy of https://www.techdrivein.com/2011/01/evolution-of-ubuntu-over-years-brief.html")

At this point [ubuntu forums](https://ubuntuforums.org) was just like a homepage to me I might as well switch to _Ubuntu_. This was back when they first introduced the famous _Ubuntu Software Center_. Also the first time I heard about the term: "The year of the Linux desktop".

## Paranoid Android
![android](https://source.android.com/static/docs/setup/images/Android_symbol_green_RGB.png)
But it wasn't the desktop that made me interested in Linux. It was about portability, especially when _android_ came into the scene. This is when I learned about _[Android Open Source Project](https://source.android.com/ "AOSP")_.

And begins my journey into _android_ _ROM_ theming, simply just decompiling, editing few files, and recompiling back. Eventually even compiling _AOSP_ from scratch.

But this quickly bores me, what if I want to create a completely custom ROM? _AOSP_ works just fine, if you happens to have the phone that actually supports it. But it felt very plain, basic. I could modify the source, but I don't know _Java_ well enough (I still don't now) to do that.

Enter [CyanogenMod](https://en.wikipedia.org/wiki/CyanogenMod "CyanogenMod"), a now discontinued open-source operating system for mobile[^1]. I love it immediately, and begin using it in all my _android_ phones.

The openness of the community made me feel like having another home, the community of tweakers. I still have an account at [androidfilehost](https://androidfilehost.com/?w=profile&uid=23134718111255102 "alexforsale"). I also [tried to compile full _Linux_ distro into android](https://forums.ubports.com/user/alexforsale).

As the _android_ version keeps adding up, it also increases the requirement for the machine used to compile it. At some point just plain _Ubuntu_ feels kinda bloated, especially for older PC that I used.

## Puzzling pieces of puzzle
![openbox](https://ubunlog.com/wp-content/uploads/2014/01/Openbox_menu.jpg "openbox")
To overcome this I started stripping my OS to the bare minimum, again I started distro hopping looking for something that lightweight enough. This is when I started using _[window manager](https://en.wikipedia.org/wiki/Window_manager "window manager")_. Suddenly GNU/Linux is like a _Lego_ brick to me, I'm putting pieces of softwares that I want, and removing the ones I don't need.

At this point, _Ubuntu_, _Debian_, and many other desktop-based distro just won't do for me. Why bother installing a distro if I'm going to uninstall many of its component after the first boot?

Fedora, OpenSuse, Gentoo, you name it, I tried them all, not to mention a quick peek every now and then to _FreeBSD_. I even compiled _[Linux from scratch](https://www.linuxfromscratch.org/ "LFS")_ if it wasn't for compiling _[rust](https://www.rust-lang.org/)_ (it literally took a day to compile in my ancient PC) it would've been my primary OS.

## instantly stable instablity
![Arch Linux](https://archlinux.org/static/archnavbar/archlogo.a2d0ef2df27d.png "Arch")
I loved Gentoo, I still am, I love their _[portage](https://wiki.gentoo.org/wiki/Portage "portage")_ system that allows me to choose what goes inside a package. But it's tiring to compile a whole toolchain just because I want to install a package that depends on it.

[Archlinux](https://archlinux.org "archlinux") is my middle ground. I only need to compile packages that aren't included in their official repository, and even then, their unofficial repository ([AUR](https://aur.archlinux.org/ "AUR")) is probably the most complete package collection known to man.

[^1]: https://lineageos.org/Yes-this-is-us/
