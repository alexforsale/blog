---
title: "IPV6 using Tunnelbroker"
slug: "ipv6-using-tunnelbroker"
date: 2023-08-26T19:48:07+07:00
description: "Adding IPv6 connectivity to an IPv4 only host."
tags: ['network', 'ipv6', 'tunnelbroker']
categories: ['linux', 'servers']
draft: false
---

# Tunnelbroker

Tunnelbroker is a service i that enables us to reach IPv6 internet by tunneling over existing IPv4 from your IPv6 enabled host or router to one of our IPv6 routers[^1]. This means that the current IPv4 connection should be static, or if it is, using another DDNS services to update our IPv4 address when changed.

Nowadays, usually most ISP already have IPv6 connection. It should be enough if we're only using it as a client, but for routers, sometimes the prefix given by the ISP isn't enough (my ISP only gives _/64_ address without any _prefix delegations_).

Tunnelbroker gives us a free routed _/64_ prefix and another additional _/48_ prefix for free. Plus it also have a free DNS service that will allows us to manage and maintain forward and reverse dns[^2].

[^1]: https://tunnelbroker.net/
[^2]: https://dns.he.net/
