---
title: "IPv6 Menggunakan Tunnelbroker"
slug: "ipv6-menggunakan-tunnelbroker"
description: "Menambahkan konektifitas IPV6."
date: 2023-08-26T19:48:12+07:00
tags: ['network', 'ipv6', 'tunnelbroker']
categories: ['linux', 'servers']
draft: false
---

# Tunnelbroker

Tunnelbroker adalah sebuah _service_ yang memungkinkan kita untuk mencapai _internet_ _IPv6_ dengan metode _tunneling_ melalui koneksi _IPv4_ yang sudah ada[^1]. Ini berarti koneksi _IPv4_ semestinya bersifat _statis_, atau jika tidak _statis_, menggunakan _service DDNS_ lain untuk meng-_update_ alamat IPv4 ketika berubah.

Umumnya sekarang ini, kebanyakan ISP sudah menyediakan koneksi _IPv6_. Ini sudah cukup jika kita mengguanakannya sebagai _client_, tapi untuk _router_, seringkali _prefix_ yang diberikan oleh _ISP_ tidak cukup (ISP yang saya gunakan hanya memberikan alamat _/64_ tanpa _prefix delegation_).

Tunnelbroker memberikan _prefix routed /64_ dan tambahan _prefix routed /48_ tambahan secara gratis. Dan lagi tambahan fitur _DNS service_ yang memungkinkan kita untuk mengatur _forward_ dan _reverse_ _dns_ [^2].

[^1]: https://tunnelbroker.net/
[^2]: https://dns.he.net/
