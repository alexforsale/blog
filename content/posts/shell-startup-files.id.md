---
title: "Shell Startup Files"
slug: "file-startup-shell"
description: "Konfigurasi file startup shell"
date: 2023-09-03T17:02:40+07:00
tags: ['shell', 'script', 'bash', 'zsh', 'csh', 'tcsh', 'stow']
categories: ['shell']
draft: false
---

## Shell
Dari [wikipedia](https://id.wikipedia.org/wiki/Syel_(komputer)):

> Dalam komputer, syel atau shell adalah program khusus yang menyediakan komunikasi langsung antara pengguna dan sistem operasi (terutama kernel). Syel aslinya adalah istilah teknis untuk baris perintah atau antarmuka grafis yang memfasilitasi interaksi dengan sistem operasi. Dinamai demikian karena syel adalah layar terluar sistem operasi yang melindungi intinya.
>
> Dalam lingkungan mirip Unix, syel lebih merujuk pada program yang menerjemahkan perintah dan antarmukanya berbentuk baris perintah.

## File - File inisialisasi Shell

_Shell_ membaca file - file konfigurasi di berbeda kondisi. File - file ini biasanya berisi perintah - perintah untuk _shell_ dan dieksekusi ketika dibuka; file - file ini biasanya digunakan untuk mengkonfigurasi variabel - variabel penting yang dipakai untuk mencari lokasi file _executable_, seperti $PATH, dan lain - lainnya yang mengatur perilaku dan tampilan dari _shell_ tersebut. Tabel dibawah ini membedakan file - file konfigurasi dari _shell - shell_ yang umum.

| File konfigurasi          | sh                  | ksh         | csh   | tcsh    | bash                 | zsh         |
|:--------------------------|:-------------------:|:-----------:|:-----:|:-------:|:--------------------:|:-----------:|
| /etc/.login               |                     |             | login | login   |                      |             |
| /etc/csh.cshrc            |                     |             | yes   | yes     |                      |             |
| /etc/csh.login            |                     |             | login | login   |                      |             |
| ~/.tcshrc                 |                     |             |       | yes     |                      |             |
| ~/.cshrc                  |                     |             | yes   | yes[^a] |                      |             |
| ~/etc/ksh.kshrc           |                     | interactive |       |         |                      |             |
| /etc/sh.shrc              | interactive[^b]     |             |       |         |                      |             |
| $ENV (typically ~/.kshrc) | interactive[^c][^d] | interactive |       |         | interactive[^e]      |             |
| ~/.login                  |                     |             | login | login   |                      |             |
| ~/.logout                 |                     |             | login | login   |                      |             |
| /etc/profile              | login               | login       |       |         | login                | login[^f]   |
| ~/.profile                | login               | login       |       |         | login[^g]            | login[^f]   |
| ~/.bash_profile           |                     |             |       |         | login[^g]            |             |
| ~/.bash_login             |                     |             |       |         | login[^g]            |             |
| ~/.bash_logout            |                     |             |       |         | login                |             |
| ~/.bashrc                 |                     |             |       |         | interactive+nonlogin |             |
| /etc/zshenv               |                     |             |       |         |                      | yes         |
| /etc/zprofile             |                     |             |       |         |                      | login       |
| /etc/zshrc                |                     |             |       |         |                      | interactive |
| /etc/zlogin               |                     |             |       |         |                      | login       |
| /etc/zlogout              |                     |             |       |         |                      | login       |
| ~/.zshenv                 |                     |             |       |         |                      | yes         |
| ~/.zprofile               |                     |             |       |         |                      | login       |
| ~/.zshrc                  |                     |             |       |         |                      | interactive |
| ~/.zlogin                 |                     |             |       |         |                      | login       |

  * kosong berarti file tersebut tidak dibaca oleh _shell_.
  * _yes_ berarti file tersebut selalu dibaca oleh _shell_ saat _startup_.

#### login shell
_Login shell_ adalah _shell_ dimana user melakukan login. Dan merupakan _process_ pertama yang dieksekusi sebagai _user ID_ dari user tersebut, _process login_ mendiktekan kepada _shell_ untuk berlaku sebagai _login shell_ dengan konvensi: memberikan argumen 0, yang normal-nya merupakan nama file _executable_ dari shell dengan tambahan awalan - (misalnya `-bash`).

Menggunakan _Bash_, kita dapat cek dengan melihat _output_ dari `$0`
```shell
echo $0
```

#### interactive shell
_Interactive shell_ adalah _shell_ yang dimulai tanpa argumen _non-option_ (kecuali `-s` ditentukan) dan tanpa memberikan opsi `-c`, dimana _input_ dan _error output_ keduanya tersambung ke terminal (ditentukan oleh isatty(3)), atau yang dimulai dengan opsi `-i`.[^1]

_Shell_ interaktif biasanya membaca dan menulis ke terminal _user_.

Invokasi opsi `-s` dapat digunakan untuk menentukan parameter _positional_ ketika _shell_ interaktif dimulai.

#### non-interactive shell
Seperti namanya, _non-interactive shell_ adalah jenis _shell_ yang tidak berinteraksi dengan _user_. Kita menjalankannya melalu _script_ atau sejenisnya. Dan juga, dijalankan melalui semacam proses yang diotomatisasi.

Dalam _Bash_, dapat kita cek dengan perintah:
```shell
[[ $- == *i* ]] && echo ‘Interactive’ || echo ‘not-interactive’
```

### file - file konfigurasi shell saya

Seperti file - file konfigurasi yang lainnya, saya menyimpan file - file inisialisasi _shell_ didalam [gitlab](https://gitlab.com/alexforsale/dotfiles-shells "dotfiles-shells"). Repository ini dibaca oleh beberapa shell yang _posix-compliance_ dan disebarkan ke direktori _home_ menggunakan [GNU Stow](https://www.gnu.org/software/stow/ "GNU Stow") sebagai _symbolic link_.

```shell
alexforsale@somalia-minimal ~/.dotfiles/shells $ tree . -la
.
├── .config
│ └── profile.d
│     ├── 00-distro.sh
│     ├── 00-locale.sh
│     ├── 01-xdg_base_directory.sh
│     ├── 02-editors.sh
│     ├── 03-terminals.sh
│     ├── 04-security.sh
│     ├── 05-filemanagers.sh
│     ├── 06-browser.sh
│     ├── 07-mail_apps.sh
│     ├── 10-polybar.sh
│     ├── 10-themes.sh
│     ├── 99-cargo.sh
│     ├── 99-ccache.sh
│     ├── 99-composer.sh
│     ├── 99-dash.sh
│     ├── 99-doom_emacs.sh
│     ├── 99-elinks.sh
│     ├── 99-emacs_vterm.sh
│     ├── 99-freebsd.sh
│     ├── 99-fsharp.sh
│     ├── 99-ghcup.sh
│     ├── 99-go.sh
│     ├── 99-guix.sh
│     ├── 99-nano.sh
│     ├── 99-nix.sh
│     ├── 99-npm.sh
│     ├── 99-password-store.sh
│     ├── 99-perl.sh
│     ├── 99-python.sh
│     ├── 99-ruby.sh
│     └── 99-screen.sh
├ .git
├ .login
├ .logout
├ .profile
├ .shrc
└── README.org

3 directories, 37 files
```

Seperti terlihat di struktur direktori diatas, saya menyimpan file - file tersebut secara modular, ini untuk mempermudah dalam pengelolaan(penambahan atau pengurangan).

#### ~/.shrc
Saya biasanya mengambil default dari _FreeBSD_ untuk file ini, dengan sedikit modifikasi.
```shell
#!/bin/sh
# $FreeBSD$
#
# .shrc - bourne shell startup file
#
# This file will be used if the shell is invoked for interactive use and
# the environment variable ENV is set to this file.
#
# see also sh(1), environ(7).
#

# file permissions: rwxr-xr-x
#
# umask	022

# Uncomment this to enable the builtin vi(1) command line editor in sh(1),
# e.g. ESC to go into visual mode.
# set -o vi

if ([ "${SHELL}" != "/bin/sh" ] && [ "${SHELL}" != "/bin/dash" ]);then
   return
fi

alias h='fc -l'
alias j=jobs
alias m="$PAGER"
alias ll='ls -laFo'
alias l='ls -l'
alias g='egrep -i'

# # be paranoid
# alias cp='cp -ip'
# alias mv='mv -i'
# alias rm='rm -i'

# set prompt: ``username@hostname:directory $ ''
if [ "${DISTRO}" = "freebsd" ]; then
    PS1="\u@\h:\w \\$ "
else
    PS1="${USER}@${HOSTNAME}:\${PWD} $ "
fi

# search path for cd(1)
# CDPATH=:$HOME
```

Modifikasi pertama:
```shell
if ([ "${SHELL}" != "/bin/sh" ] && [ "${SHELL}" != "/bin/dash" ]);then
   return
fi
```
Ini mamastikan file ini tidak akan di-_source_ jika _shell_ yang berjalan bukan "/bin/sh" atau "/bin/dash", karena hanya kompatibel dengan kedua itu saja.

Yang kedua adalah:
```shell
if [ "${DISTRO}" = "freebsd" ]; then
    PS1="\u@\h:\w \\$ "
else
    PS1="${USER}@${HOSTNAME}:\${PWD} $ "
fi
```

Ini akan menentukan variable _PS1_ berdasarkan isi dari variabel _$DISTRO_.

#### ~/.login
```shell
#!/usr/bin/env tcsh
# -*- mode: sh -*-
# $FreeBSD$
#
# .login - csh login script, read by login shell, after `.cshrc' at login.
#
# See also csh(1), environ(7).
#

# Query terminal size; useful for serial lines.
if ( -x /usr/bin/resizewin ) /usr/bin/resizewin -z
```

File ini dibaca oleh _tcsh_ dan _csh_, dan merupakan default dari _FreeBSD_. Perintah `resizewin` adalah tool untuk mengecek dan menentukan ukuran terminal (lihat [manpage](https://man.freebsd.org/cgi/man.cgi?resizewin(1)) untuk penjelasan detailnya).

#### ~/.logout
```shell
#!/usr/bin/env tcsh
# -*- mode: sh -*-
#
# .logout - csh logout script
#

if ( -x `which clear` ) clear
```

Ketika menggunakan _tcsh_ atau _csh_, file ini memastikan isi dari terminal dibersihkan ketika _user_ _logout_ dari terminal.

#### ~/.profile
Ini merupakan bagian terpentingnya.

Bagian ini mengecek dan _source_ file _/etc/profile_ jika ditemukan.
```shell
#!/bin/sh
# ~/.profile
# Environment and startup programs.
# source /etc/profile if exist.
# <alexforsale@yahoo.com>
#

[ -f /etc/profile ] && . /etc/profile
```

Jika ada file _$HOME/bin_ dan/atau _$HOME/.local/bin_, akan dimasukkan kedalam variabel _$PATH_.
```shell
# this goes first in case others needs it.
if [ -d "${HOME}/bin" ] ; then
    export PATH="${HOME}/bin:${PATH}"
fi
if [ -d "$HOME/.local/bin" ];then
   export PATH="${HOME}/.local/bin:${PATH}"
fi
```
Bagian ini akan melakukan _source_ file - file di dalam _$HOME/.config/profile.d_. Urutannya ditentukan oleh nama file, jadi biasanya saya menggunakan penamaan _[nomo]-[namafile].sh_ untuk memastikan urutan pemanggilannya.
```shell
# Loads user profiles if exists. Should be in ~/.profile.d
# but let's not pollute ~ anymore.

if [ -d "${HOME}/.config/profile.d" ]; then
    for profile in "${HOME}"/.config/profile.d/*.sh; do
        . "${profile}"
    done
    unset profile
fi
```
Saya lupa dapat bagian ini darimana, tapi kode keren ini berfungsi membersihkan variabel _$PATH_ dari duplikasi.
```shell
if [ -n "${PATH}" ]; then
    old_PATH=${PATH}:; PATH=
    while [ -n "${old_PATH}" ]; do
        x=${old_PATH%%:*}       # the first remaining entry
        case ${PATH}: in
            *:"$x":*) ;;         # already there
            *) PATH=${PATH}:$x;;    # not there yet
        esac
        old_PATH=${old_PATH#*:}
    done
    PATH=${PATH#:}
    unset old_PATH x
fi
```
Bagian ini akan me-_source_ _$HOME/.config/profile.local_ atau _$HOME/.profile.local_ jika ditemukan. Sengaja ditempatkan belakangan agar dapat mengganti variabel - variabel yang diset sebelumnya.
```shell
# local ~/.profile
if [ -r "${HOME}"/.config/profile.local ];then
   . "${HOME}"/.config/profile.local
   elif [ -r "${HOME}"/.profile.local ];then
        . "${HOME}"/.profile.local
   fi
```
Khusus jika menggunakan _Bash_, bagian ini akan lanjut me-_source_ _$HOME/.bashrc_ seandainya _$HOME/.bash_profile_ atau _$HOME/.bash_login_ tidak ditemukan atau tidak me-_source_ file _$HOME/.bashrc_.
```shell
# if running bash
if [ -n "${BASH_VERSION}" ]; then
    # include .bashrc if it exists
    if [ -f "${HOME}/.bashrc" ]; then
        . "${HOME}/.bashrc"
    fi
fi
```

#### sisanya
Tidak semuanya ditampilkan (dapat dilihat langsung di[repository](https://gitlab.com/alexforsale/dotfiles-shells)), karena umumnya diambil langsung dari wiki atau dokumentasi resmi. Beberapa yang cukup menarik:

##### ~/.config/profile.d/00-distro.sh
File ini menentukan variabel _DISTRO_ dan _DISTROVER_ berdasarkan sistem operasi.
```shell
if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    DISTRO="${ID}"
    DISTROVER="${VERSION_ID}"
    [ -z "${DISTROVER}" ] &&
        DISTROVER="${BUILD_ID}"
elif [ "$(command -v lsb_release >/dev/null)" ]; then
    # linuxbase.org
    DISTRO="$(lsb_release -si | awk '{print tolower ($0)}')"
    DISTROVER="$(lsb_release -sr | awk '{print tolower ($0)}')"
elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    DISTRO="${DISTRIB_ID}"
    DISTROVER="${DISTRIB_RELEASE}"
elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    DISTRO=Debian
    DISTROVER="$(cat /etc/debian_version)"
else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    DISTRO="$(uname -s)"
    DISTROVER="$(uname -r)"
fi
```

[^a]: hanya jika file ~/.tcshrc tidak ada
[^b]: hanya di Bourne Shells versi baru
[^c]: tersedia di sistem - sistem yang mendukung "User Portability Utilities option"
[^d]: $ENV adalah $HOME/.shrc di versi baru Bourne Shell
[^e]: perilaku yang sama dengan _sh_, tapi hanya jika dipanggil sebagai _sh_ (_bash_ versi 2+) atau, sejak _bash_ versi 4.2, jika dipanggil secara eksplisit dalam _POSIX compatibility mode_ (dengan opsi --posix atau -o posix).
[^f]: hanya di mode kompatibilitas sh/ksh (ketika dipanggil sebagai _bash_, _sh_, atau _ksh_)
[^g]: file pertama yang dapat dibaca dengan urutan ~/.bash_profile, ~/.bash_login, dan ~/.profile; dan hanya ~/.profile jika dipanggil sebagai _sh_ atau, sejak _bash_ versi 4.2, jika dipanggil secara eksplisit dalam _POSIX compatibility mode_ (dengan opsi --posix atau -o posix)
