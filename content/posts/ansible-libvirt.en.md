---
title: "Manage Libvirt using Ansible"
slug: "manage-libvirt-using-ansible"
description: "Create and manage virtual machine using ansible."
date: 2023-08-28T13:56:19+07:00
tags: ['ansible', 'libvirt']
categories: ['ansible', 'servers']
draft: False
---

# KVM
I recently made an ansible role for managing **[KVM](https://wiki.archlinux.org/title/KVM)** (kernel-based virtual machine) [here](https://github.com/alexforsale/ansible_homelab). Currently it's for _Arch Linux_-based distribution as the KVM host, but it can easily adapted for other distribution. The goal is to automate the creation and modification of virtual machine using _ansible_.

## Example Playbook

```yaml
- name: Deploys VMs
  hosts: localhost
  gather_facts: yes
  become: yes
  vars:
    libvirt:
      host_username: "{{ lookup('env', 'USER') }}"
      pool:
        - name: ansible_iso_dir
          type: dir
          path: "{{ iso_dir }}"
          mode: "0755"
          owner: 0
          group: 0
          autostart: True
          iso: True
        - name: default
          type: dir
          path: "{{ storage_dir }}"
          mode: "0755"
          owner: 0
          group: 0
          autostart: True
          iso: False
      net:
        - name: ansible_network
          forward_mode: nat
          bridge_name: virt_net1
          stp: "on"
          delay: 0
          ip_address: 10.0.33.254
          netmask: 255.255.255.0
          dhcp_start: 10.0.33.100
          dhcp_end: 10.0.33.200
          autostart: True
      vm:
        - name: freebsd
          arch: 'x86_64'
          distro: freebsd
          vcpus: 1
          machine: 'pc-q35-8.1'
          mode: 'host-passthrough'
          emulator: "/usr/bin/qemu-system-x86_64"
          default_disk_driver_type: qcow2
          default_disk_dev: vda
          default_disk_bus: virtio
          ram_mb: 1024
          root_password: test1234
          network: ansible_network
          network_type: virtio
          graphics:
            - type: "spice"
          videos:
            - type: qxl
          memballoon_model: "virtio"
          ssh_pub_key: "/home/alexforsale/.ssh/id_rsa.pub"
        - name: debian
          arch: 'x86_64'
          distro: debian
          vcpus: 1
          machine: 'pc-q35-8.1'
          mode: 'host-passthrough'
          emulator: "/usr/bin/qemu-system-x86_64"
          default_disk_driver_type: qcow2
          default_disk_dev: vda
          default_disk_bus: virtio
          ram_mb: 1024
          root_password: test1234
          network: ansible_network
          network_type: virtio
          graphics:
            - type: "spice"
          videos:
            - type: qxl
          memballoon_model: "virtio"
          ssh_pub_key: "/home/alexforsale/.ssh/id_rsa.pub"
        - name: arch
          arch: 'x86_64'
          distro: archlinux
          vcpus: 1
          machine: 'pc-q35-8.1'
          mode: 'host-passthrough'
          emulator: "/usr/bin/qemu-system-x86_64"
          default_disk_driver_type: qcow2
          default_disk_dev: vda
          default_disk_bus: virtio
          ram_mb: 1024
          root_password: test1234
          network: ansible_network
          network_type: virtio
          graphics:
            - type: "spice"
          videos:
            - type: qxl
          memballoon_model: "virtio"
          ssh_pub_key: "/home/alexforsale/.ssh/id_rsa.pub"

  tasks:
    - name: KVM Provision role
      include_role:
        name: kvm_provision
      tags: libvirt

```

Currently I've tested this role in my laptop with 3 virtual machines (debian, freebsd and archlinux).

![ansible](/images/2023-09-02_14-56-ansible-libvirt-cli.png)

![virt-manager](/images/2023-09-02_14-56-virt-manager-ansible-libvirt.png)
