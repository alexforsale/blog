---
title: "Mengelola Libvirt menggunakan Ansible"
slug: "mengelola-libvirt-menggunakan-ansible"
description: "Membuat dan mengelola virtual machine menggunakan ansible"
date: 2023-08-28T13:56:22+07:00
tags: ['ansible', 'libvirt']
categories: ['ansible', 'servers']
draft: False
---

# KVM
Baru - baru ini saya membuat sebuah _ansible role_ untuk mengelola **[KVM](https://wiki.archlinux.org/title/KVM)** (kernel-based virtual machine) [here](https://github.com/alexforsale/ansible_homelab). Sementara ini baru untuk distribusi berbasis _Arch Linux_ sebagai _host_-nya, tapi bisa dengan mudah diadaptasi untuk distribusi lainnya. Tujuan utama-nya adalah untuk otomatisasi penciptaan dan modifikasi _virtual machine_ menggunakan _ansible_.

## Example Playbook

```yaml
- name: Deploys VMs
  hosts: localhost
  gather_facts: yes
  become: yes
  vars:
    libvirt:
      host_username: "{{ lookup('env', 'USER') }}"
      pool:
        - name: ansible_iso_dir
          type: dir
          path: "{{ iso_dir }}"
          mode: "0755"
          owner: 0
          group: 0
          autostart: True
          iso: True
        - name: default
          type: dir
          path: "{{ storage_dir }}"
          mode: "0755"
          owner: 0
          group: 0
          autostart: True
          iso: False
      net:
        - name: ansible_network
          forward_mode: nat
          bridge_name: virt_net1
          stp: "on"
          delay: 0
          ip_address: 10.0.33.254
          netmask: 255.255.255.0
          dhcp_start: 10.0.33.100
          dhcp_end: 10.0.33.200
          autostart: True
      vm:
        - name: freebsd
          arch: 'x86_64'
          distro: freebsd
          vcpus: 1
          machine: 'pc-q35-8.1'
          mode: 'host-passthrough'
          emulator: "/usr/bin/qemu-system-x86_64"
          default_disk_driver_type: qcow2
          default_disk_dev: vda
          default_disk_bus: virtio
          ram_mb: 1024
          root_password: test1234
          network: ansible_network
          network_type: virtio
          graphics:
            - type: "spice"
          videos:
            - type: qxl
          memballoon_model: "virtio"
          ssh_pub_key: "/home/alexforsale/.ssh/id_rsa.pub"
        - name: debian
          arch: 'x86_64'
          distro: debian
          vcpus: 1
          machine: 'pc-q35-8.1'
          mode: 'host-passthrough'
          emulator: "/usr/bin/qemu-system-x86_64"
          default_disk_driver_type: qcow2
          default_disk_dev: vda
          default_disk_bus: virtio
          ram_mb: 1024
          root_password: test1234
          network: ansible_network
          network_type: virtio
          graphics:
            - type: "spice"
          videos:
            - type: qxl
          memballoon_model: "virtio"
          ssh_pub_key: "/home/alexforsale/.ssh/id_rsa.pub"
        - name: arch
          arch: 'x86_64'
          distro: archlinux
          vcpus: 1
          machine: 'pc-q35-8.1'
          mode: 'host-passthrough'
          emulator: "/usr/bin/qemu-system-x86_64"
          default_disk_driver_type: qcow2
          default_disk_dev: vda
          default_disk_bus: virtio
          ram_mb: 1024
          root_password: test1234
          network: ansible_network
          network_type: virtio
          graphics:
            - type: "spice"
          videos:
            - type: qxl
          memballoon_model: "virtio"
          ssh_pub_key: "/home/alexforsale/.ssh/id_rsa.pub"

  tasks:
    - name: KVM Provision role
      include_role:
        name: kvm_provision
      tags: libvirt
  tasks:
    - name: KVM Provision role
      include_role:
        name: kvm_provision
      tags: libvirt
```

Saat ini saya sudah mencoba _role_ ini di _laptop_ dengan menggunakan 3 _virtual machine (debian, freebsd and archlinux)_.

![ansible](/images/2023-09-02_14-56-ansible-libvirt-cli.png)

![virt-manager](/images/2023-09-02_14-56-virt-manager-ansible-libvirt.png)
