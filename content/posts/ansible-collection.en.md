---
title: "Ansible Collection"
slug: "ansible-collection"
description: "Creating ansible collection."
date: 2023-08-27T12:23:40+07:00
tags: ['ansible', 'git', 'github']
categories: ['ansible']
draft: false
---
# Ansible Collection
-------------------------------------------------------------------------------
Collections are a distribution format for Ansible content that can include playbooks, roles, modules, and plugins. You can install and use collections through a distribution server, such as Ansible Galaxy, or a Pulp 3 Galaxy server[^1].

The common uses of _collection_ is by browsing the installed collection in _[Ansible Galaxy](https://galaxy.ansible.com/)_, by default it is https://galaxy.ansible.com/,

```shell
ansible-galaxy collection list | head -n 20
```

```
# /usr/lib/python3.11/site-packages/ansible_collections
Collection                    Version
----------------------------- -------
amazon.aws                    6.3.0
ansible.netcommon             5.1.2
ansible.posix                 1.5.4
ansible.utils                 2.10.3
ansible.windows               1.14.0
arista.eos                    6.0.1
awx.awx                       22.6.0
azure.azcollection            1.16.0
check_point.mgmt              5.1.1
chocolatey.chocolatey         1.5.1
cisco.aci                     2.7.0
cisco.asa                     4.0.1
cisco.dnac                    6.7.3
cisco.intersight              1.0.27
cisco.ios                     4.6.1
cisco.iosxr                   5.0.3
```
## Create account
This is necessary if you want to publish your collection to the galaxy server. The [default server](https://galaxy.ansible.com/ "ansible galaxy") account creation is just simply connecting your existing _github_ account.
## Creating Collection

This command (`ansible-galaxy collection init`) will create a collection with the `[namespace]/[collection_name]` structure. The `[namespace]` is set at the `My Content/Edit Namespace` tab in the web interface. The syntax for creating the collection itself is `ansible-galaxy collection init <[namespace].[collection_name]>`.
![galaxy namespace](/images/2023-08-27_13-40-galaxy-namespace.png)

```shell
ansible-galaxy collection init alexforsale.homelab
```

```
- Collection alexforsale.homelab was created successfully
```

Inside the `[namespace]/[collection_name]`

```shell
tree alexforsale/homelab/
```

```
alexforsale/homelab/
├── README.md
├── docs
├── galaxy.yml
├── meta
│   └── runtime.yml
├── plugins
│   └── README.md
└── roles

5 directories, 4 files
```

This is the _skeleton_ structure to build on. And it is better to put it under version control, I'm using git here.

```shell
cd alexforsale/homelab
git init
```

```
Initialized empty Git repository in /home/alexforsale/Projects/ansible/alexforsale/homelab/.git/
```

## Publishing your collection

To actually publish your collection into the galaxy server, we must first build it using the `ansible-galaxy build` command with the syntax:

    usage: ansible-galaxy collection build [-h] [-s API_SERVER] [--token API_KEY] [-c] [--timeout TIMEOUT] [-v] [-f] [--output-path OUTPUT_PATH] [collection ...]

by default, it will search the current working directory for a `galaxy.yml` file. So it can be used inside the git repository we just created earlier.

Modify `galaxy.yml` to reflect the actual user, namespace, collection name, authors, etc. Also uncomment the `# requires_ansible:` line at `./meta/runtime.yml`.

```shell
git add .
git commit -m 'skeleton init'
```

```
[main (root-commit) 773eec1] skeleton init
 4 files changed, 155 insertions(+)
 create mode 100644 README.md
 create mode 100644 galaxy.yml
 create mode 100644 meta/runtime.yml
 create mode 100644 plugins/README.md
```

Committing the current state of the repository. And better to ignore the tarball created by the `ansible-galaxy publish` command too.

```shell
echo '*.tar.gz' >> .gitignore
git add .gitignore
git commit -m 'add .gitignore'
```

```
[main dec7ede] add .gitignore
 1 file changed, 1 insertion(+)
 create mode 100644 .gitignore
```

And the actual build:
```shell
ansible-galaxy collection build
```

```
Created collection for alexforsale.homelab at /home/alexforsale/Projects/ansible/alexforsale/homelab/alexforsale-homelab-1.0.0.tar.gz
```

### galaxy_token ###

We can provide the token within the command line, but since I'm only using one account for galaxy server, it's better to put it inside `~/.ansible/galaxy_token`, this is the default location _ansible-galaxy_ will look for token.

The token itself is created from the web interface.

```shell
mkdir -pv ~/.ansible
touch ~/.ansible/galaxy_token
echo "token: <your_ansible_galaxy_token>" > ~/.ansible/galaxy_token
```

And the actual publishing:
```shell
ansible-galaxy collection publish alexforsale-homelab-1.0.0.tar.gz
```

```
Publishing collection artifact '/home/alexforsale/Projects/ansible/alexforsale/homelab/alexforsale-homelab-1.0.0.tar.gz' to default https://galaxy.ansible.com/api/
Collection has been published to the Galaxy server default https://galaxy.ansible.com/api/
Waiting until Galaxy import task https://galaxy.ansible.com/api/v2/collection-imports/30907/ has completed
Collection has been successfully published and imported to the Galaxy server default https://galaxy.ansible.com/api/
```

![imported collection](/images/2023-08-27_13-31-ansible-galaxy-import.png)

For those who are interested, this collection is also available at [github](https://github.com/alexforsale/ansible_homelab "github").

[^1]: https://docs.ansible.com/ansible/latest/collections_guide/index.html
