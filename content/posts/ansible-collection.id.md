---
title: "Ansible Collection"
slug: "ansible-collection"
description: "Mengelola collection menggunakan ansible."
date: 2023-08-27T12:23:42+07:00
tags: ['ansible', 'git', 'github']
categories: ['ansible']
draft: false
---
# Ansible Collection
-------------------------------------------------------------------------------
_Collection_ adalah format distribusi untuk konten _Ansible_ yang dapat meliputi _playbooks_, _roles_, _modules_, dan _plugins_. Kita dapat meng-_install_ dan menggunakan _collection_ ini melalui _distribution server_, seperti misalkan _Ansible Galaxy_, atau _Pulp 3 Galaxy Server_ [^1].

Penggunaan umum dari _collection_ ini adalah dengan melakukan pencarian di _[Ansible Galaxy](https://galaxy.ansible.com/)_, yang _default_-nya berada di https://galaxy.ansible.com/.

```shell
ansible-galaxy collection list | head -n 20
```

```
# /usr/lib/python3.11/site-packages/ansible_collections
Collection                    Version
----------------------------- -------
amazon.aws                    6.3.0
ansible.netcommon             5.1.2
ansible.posix                 1.5.4
ansible.utils                 2.10.3
ansible.windows               1.14.0
arista.eos                    6.0.1
awx.awx                       22.6.0
azure.azcollection            1.16.0
check_point.mgmt              5.1.1
chocolatey.chocolatey         1.5.1
cisco.aci                     2.7.0
cisco.asa                     4.0.1
cisco.dnac                    6.7.3
cisco.intersight              1.0.27
cisco.ios                     4.6.1
cisco.iosxr                   5.0.3
```
## Pembuatan Akun
Hal ini diperlukan jika ingin menerbitkan _collection_ ke _galaxy server_. Pembuatan akun di [server default](https://galaxy.ansible.com/ "ansible galaxy") bisa dengan menautkan akun _github_ kita.

## Creating Collection
Perintah ini (`ansible-galaxy collection init`) akan menciptakan _collection_ baru dengan struktur `[namespace]/[collection_name]`. `[namespace]` ditentukan didalam menu `My Content/Edit Namespace` didalam _web interface_. Syntax untuk membuat _collection_ adalah `ansible-galaxy collection init <[namespace].[collection_name]>`
![galaxy namespace](/images/2023-08-27_13-40-galaxy-namespace.png)

```shell
ansible-galaxy collection init alexforsale.homelab
```

```
- Collection alexforsale.homelab was created successfully
```

Didalam `[namespace]/[collection_name]`

```shell
tree alexforsale/homelab/
```

```
alexforsale/homelab/
├── README.md
├── docs
├── galaxy.yml
├── meta
│   └── runtime.yml
├── plugins
│   └── README.md
└── roles

5 directories, 4 files
```

Ini adalah struktur _skeleton_ yang akan menjadi basis _collection_. Dan baiknya berada didalam _version control_. Saya disini menggunakan _git_.

```shell
cd alexforsale/homelab
git init
```

```
Initialized empty Git repository in /home/alexforsale/Projects/ansible/alexforsale/homelab/.git/
```

## Publishing your collection
Untuk menerbitkan _collection_ ini kedalam _galaxy server_, kita perlu 'membungkusnya' terlebih dahulu menjadi _tarball_ dengan menggunakan perintah `ansible-galaxy build` yang _syntax_-nya:

    usage: ansible-galaxy collection build [-h] [-s API_SERVER] [--token API_KEY] [-c] [--timeout TIMEOUT] [-v] [-f] [--output-path OUTPUT_PATH] [collection ...]

_Default_-nya perintah ini akan mencari didalam direktori untuk file `galaxy.yml`. Jadi perintah ini dapat dipanggil didalam repositori _git_ yang baru saja dibuat diatas.

Modifikasi file `galaxy.yml` dan sesuaikan _user_, _namespace_, _collection name_, _authors_, dan lainnya. Juga hapus komen dari baris `# requires_ansible:` di _file_ `./meta/runtime.yml`.

```shell
git add .
git commit -m 'skeleton init'
```

```
[main (root-commit) 773eec1] skeleton init
 4 files changed, 155 insertions(+)
 create mode 100644 README.md
 create mode 100644 galaxy.yml
 create mode 100644 meta/runtime.yml
 create mode 100644 plugins/README.md
```
_Commit_ _state_ repositori saat ini, bagusnya juga _file tarball_ yang dibuat oleh `ansible-galaxy publish` di_ignore_ sehingga tidak termasuk kedalam _version control_.

```shell
echo '*.tar.gz' >> .gitignore
git add .gitignore
git commit -m 'add .gitignore'
```

```
[main dec7ede] add .gitignore
 1 file changed, 1 insertion(+)
 create mode 100644 .gitignore
```

Dan untuk proses _build_-nya:

```shell
ansible-galaxy collection build
```

```
Created collection for alexforsale.homelab at /home/alexforsale/Projects/ansible/alexforsale/homelab/alexforsale-homelab-1.0.0.tar.gz
```

### galaxy_token ###

_Token_ yang dibutuhkan dapat dicantumkan didalam perintah _command line_. Tapi karena disini saya hanya menggunakan satu akun, lebih mudah untuk menyimpannya didalam `~/.ansible/galaxy_token`, ini adalah lokasi _default_ yang dicari oleh perintah _ansible-galaxy_.

_Token_-nya dapat kita lihat melalui _web interface_.

```shell
mkdir -pv ~/.ansible
touch ~/.ansible/galaxy_token
echo "token: <your_ansible_galaxy_token>" > ~/.ansible/galaxy_token
```

And the actual publishing:
```shell
ansible-galaxy collection publish alexforsale-homelab-1.0.0.tar.gz
```

```
Publishing collection artifact '/home/alexforsale/Projects/ansible/alexforsale/homelab/alexforsale-homelab-1.0.0.tar.gz' to default https://galaxy.ansible.com/api/
Collection has been published to the Galaxy server default https://galaxy.ansible.com/api/
Waiting until Galaxy import task https://galaxy.ansible.com/api/v2/collection-imports/30907/ has completed
Collection has been successfully published and imported to the Galaxy server default https://galaxy.ansible.com/api/
```

![imported collection](/images/2023-08-27_13-31-ansible-galaxy-import.png)

Bagi mereka yang tertarik, _collection_ ini juga tersedia di [github](https://github.com/alexforsale/ansible_homelab "github").

[^1]: https://docs.ansible.com/ansible/latest/collections_guide/index.html
