---
title: "Hugo Gitlab-CI Deployment"
slug: "hugo-gitlab-ci-deployment"
date: 2023-08-25T10:06:35+07:00
description: "Deploying Hugo static sites using gitlab-ci."
tags: ['hugo', 'gitlab', 'git', 'gitlab-runners']
categories: ['Web']
draft: false
---

## Hugo
This blog uses [Hugo](https://gohugo.io "hugo") as a framework and gitlab's [CI](https://docs.gitlab.com/ee/ci/ "gitlab CI") for deployments, this means that this blog is fully [version-controlled](https://gitlab.com/alexforsale/blog "gitlab repo").

### Gitlab
I won't go too much into details about gitlab CI/CD, there's already enough documentation from [gitlab](https://docs.gitlab.com/), so I'll only documents whats I used for this blog.
### Runners?
GitLab Runner is an application that works with GitLab CI/CD to run jobs in a pipeline.[^1] For this blog I'm using a self-managed runners, but by default, the runners used for all gitlab projects are from [SaaS runners](https://docs.gitlab.com/ee/ci/runners/index.html "SaaS Runners") hosted by gitlab.

#### Self-Managed
![Assign Runners](/images/2023-08-25_10-40-gitlab-runners-assigned.png)
Extra steps needed to use a self-managed gitlab runners.
  * I need to [install](https://docs.gitlab.com/runner/install/index.html "install runners") gitlab-runners to the infrastructures that I managed.
  * Assign those runners in the project settings.

### CI configuration
-------------------------------------------------------------------------------
The default configuration file for CI/CD is **.gitlab-ci.yml**, located in the root directory of the project, but can be customized inside each project settings.

![pipeline list](/images/2023-08-26_10-33-gitlab-runningl-pipeline.png "pipeline lists")

#### Creating pipeline

##### Using variables
CI/CD variables are a type of environment variable.[^3] We can use them to:
- Control the behavior of jobs and pipelines.
- Store values you want to re-use.
- Avoid hard-coding values in your `.gitlab-ci.yml` file.

I'm storing variables inside the repository settings.
![gitlab-ci variables](/images/2023-08-26_11-23-gitlab-ci-variables.png "gitlab-ci variables")
We can define variable type as a `variable` (just like ordinary `shell environment variables`) or as a `file`.

To add variables inside our `.gitlab-ci.yml` file, use `variable` at the top-level:
```yaml
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  GIT_SUBMODULE_DEPTH: 1
```

Here the `GIT_SUBMODULE_STRATEGY` and `GIT_SUBMODULE_DEPTH` are [predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html "predefined variables") that are available in every Gitlab CI/CD.

##### Configuring `.gitlab-ci.yml`
```yaml
before_script:
  - eval $(ssh-agent -s)
  - mkdir -p ~/.ssh
  - ssh-add <(echo "$SSH_PRIVATE_KEY")
  - touch ~/.ssh/known_hosts
  - ssh-keyscan $SSH_HOST >> ~/.ssh/known_hosts
  - chmod 644 ~/.ssh/known_hosts
  - chmod 700 ~/.ssh
```

`before_script` Override a set of commands that are executed before job.

  `eval $(ssh-agent -s)` generate a _bourne shell_ commands as output.

  `mkdir -p ~/.ssh` creates the `~/.ssh` at the runners machine.

  `ssh-add <(echo "$SSH_PRIVATE_KEY")` adds the `$SSH_PRIVATE_KEY` variable into _ssh_.

  `touch ~/.ssh/known_hosts` updates or creates the access time and modification time of the `~/.ssh/known_hosts` file.

  `ssh-keyscan $SSH_HOST >> ~/.ssh/known_hosts` scan the `$SSH_HOST` variable for public _ssh host keys_ and pipe it into `~/.ssh/known_hosts`.

  `chmod 644 ~/.ssh/known_hosts` and `chmod 700 ~/.ssh` set the permissions.

```yaml
stages:
  - build
  - deploy

job_build:
  stage: build
  tags:
    - hugo
  script:
    - hugo --noTimes --noChmod --verbose
  artifacts:
    paths:
      - ./public/
  environment: production

job_deploy:
  stage: deploy
  tags:
    - hugo
  script:
    - ssh $SSH_USER_NAME@$SSH_HOST rm -rf $SITE_DIRECTORY/*
    - rsync -avuz ./public/* $SSH_USER_NAME@$SSH_HOST:$SITE_DIRECTORY
  environment: production
```

  `tags` selects the runners.

  `script` is a shell script that is executed by the runners.


The `job_build` and `job_deploy` is used to deploy this blog to my web server designated in the `SSH_HOST` variable.

#### Pipeline Schedule
![pipeline schedule](/images/2023-08-26_10-33-gitlab-pipeline-schedule.png "schedule editor")
With a scheduled pipeline we can run Gitlab CI/CD at regular intervals.[^2]
* *Descriptions*

  This pipeline description.

* *Interval Patterns*

  Pipeline interval using [cron notation](https://docs.gitlab.com/ee/topics/cron/index.html "cron notation").

* *Cron timezone*

  The timezone on which the pipeline will be based on.

* *Select target branch or tag*

  The branch or tags that will be built by the pipeline.

* *Variables*

  Additional variables for the pipeline.

![pipeline schedule](/images/2023-08-26_10-33-gitlab-pipeline-all-schedule.png "scheduled pipeline")

[^1]: https://docs.gitlab.com/runner
[^2]: https://docs.gitlab.com/ee/ci/pipelines/schedules.html
[^3]: https://docs.gitlab.com/ee/ci/variables/
