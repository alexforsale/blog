---
title: "Managing Tunnelbroker using Ansible"
slug: "managing-tunnelbroker-using-ansible"
description: "Managing Tunnelbroker using Ansible."
date: 2023-08-27T12:12:05+07:00
tags: ['network', 'ipv6', 'tunnelbroker', 'ansible', 'homelab']
categories: ['linux', 'servers', 'ansible']
draft: true
---

# Ansible
This is part of a series of posts about managing _homelab_ using _ansible_.
