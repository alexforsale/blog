---
title: "Deployment Hugo Menggunakan Gitlab-CI"
slug: "deployment-hugo-menggunakan-gitlab-ci"
date: 2023-08-25T10:06:38+07:00
description: "Deploying Hugo static sites menggunakan gitlab-ci."
tags: ['hugo', 'gitlab', 'git', 'gitlab-runners']
categories: ['Web']
draft: false
---
## Hugo
Blog ini menggunakan [Hugo](https://gohugo.io "hugo") sebagai _framework_ and gitlab [CI](https://docs.gitlab.com/ee/ci/ "gitlab CI") untuk _deployments_, Ini juga berarti blog ini sepenuhnya di _[version-control](https://gitlab.com/alexforsale/blog "gitlab repo")_.

### Gitlab
Saya tidak akan menjelaskan terlalu detail tentang gitlab CI/CD, sudah banyak dokumentasi di [gitlab](https://docs.gitlab.com/), jadi saya rasa disini saya cukup mendokumentasikan yang saya gunakan untuk blog ini saja.
### Runners?
GitLab Runner adalah aplikasi yang digunakan oleh GitLab CI/CD untuk menjalankan _jobs_ didalam _pipeline_.[^1] Untuk blog ini saya menggunakan _self-managed runners_, tapi _default_-nya, _runners_ yang dipakai untuk semua _project_ di _gitlab_ berasal dari [SaaS runners](https://docs.gitlab.com/ee/ci/runners/index.html "SaaS Runners") yang di _host_ oleh gitlab.

#### Self-Managed
![Assign Runners](/images/2023-08-25_10-40-gitlab-runners-assigned.png)
Langkah - langkah tambahan yang diperlukan ketika menggunakan _self-managed gitlab runners_.
  * [Install](https://docs.gitlab.com/runner/install/index.html "install runners") _gitlab-runners_ ke semua infrastruktur yang dipakai(server, docker dll).
  * Menugaskan semua _runners_ didalam _project settings_.

### CI configuration
File konfigurasi default untuk _CI/CD_ adalah **.gitlab-ci.yml** yang berada didalam directori utama dari _project_, namun dapat dikustomisasi didalam _settings project_.

![pipeline list](/images/2023-08-26_10-33-gitlab-runningl-pipeline.png "pipeline lists")

#### Membuat _pipeline_

##### Penggunaan _variables_
_Variable_ **CI/CD** merupakan tipe _environment variable_.[^3] Dan penggunaannya untuk:
- Mengontrol perilaku dari _job_ and _pipeline_.
- Menyimpan _values_ agar dapat digunakan ulang.
- Menghindari penyimpanan langsung dari _values_ didalam _file_ `.gitlab-ci.yml`.

Disini saya menyimpan _variables_ didalam _settings repository_ dalam _gitlab_.
![gitlab-ci variables](/images/2023-08-26_11-23-gitlab-ci-variables.png "gitlab-ci variables")
Kita dapat mendefinisikan tipe sebuah _variable_ sebagai `variable` (seperti `shell environment variables` pada umumnya) atau sebagai sebuah `file`.

Untuk menambahkan _variables_ didalam _file_ `.gitlab-ci.yml`, gunakan _keyword_ `variable` di level teratas:
```yaml
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  GIT_SUBMODULE_DEPTH: 1
```

disini _variable_ `GIT_SUBMODULE_STRATEGY` dan `GIT_SUBMODULE_DEPTH` merupakan [predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html "predefined variables") yang otomatis tersedia didalam setiap _Gitlab CI/CD_.

##### Konfigurasi `.gitlab-ci.yml`
```yaml
before_script:
  - eval $(ssh-agent -s)
  - mkdir -p ~/.ssh
  - ssh-add <(echo "$SSH_PRIVATE_KEY")
  - touch ~/.ssh/known_hosts
  - ssh-keyscan $SSH_HOST >> ~/.ssh/known_hosts
  - chmod 644 ~/.ssh/known_hosts
  - chmod 700 ~/.ssh
```

`before_script` Merupakan sekumpulan _commands_ yang dieksekusi sebelum setiap _job_ dimulai.

  `eval $(ssh-agent -s)` Menghasilkan _output_ berupa _commands_ yang kompatibel dengan skrip _bourne shell_.

  `mkdir -p ~/.ssh` membuat direktori `~/.ssh` jika belum ada di setiap _runners_.

  `ssh-add <(echo "$SSH_PRIVATE_KEY")` menambahkan isi dari variabel `$SSH_PRIVATE_KEY` kedalam file konfigurasi _ssh_.

  `touch ~/.ssh/known_hosts` memperbaharui (atau jika belum ada, membuat) waktu akses dan modifikasi dari _file_ `~/.ssh/known_hosts`.

  `ssh-keyscan $SSH_HOST >> ~/.ssh/known_hosts` memindai `$SSH_HOST` untuk _ssh host keys_ publik dan menyimpannya kedalam `~/.ssh/known_hosts`.

  `chmod 644 ~/.ssh/known_hosts` dan `chmod 700 ~/.ssh` mengatur _permission_ dari _file_ dan direktori.

```yaml
stages:
  - build
  - deploy

job_build:
  stage: build
  tags:
    - hugo
  script:
    - hugo --noTimes --noChmod --verbose
  artifacts:
    paths:
      - ./public/
  environment: production

job_deploy:
  stage: deploy
  tags:
    - hugo
  script:
    - ssh $SSH_USER_NAME@$SSH_HOST rm -rf $SITE_DIRECTORY/*
    - rsync -avuz ./public/* $SSH_USER_NAME@$SSH_HOST:$SITE_DIRECTORY
  environment: production
```

  `tags` memilih _runners_ yang akan digunakan.

  `script` merupakan skrip _shell_ yang akan dieksekusi oleh _runner_.


`job_build` dan `job_deploy` digunakan untuk men-_deploy_ _blog_ ini ke _web server_ yang ditentukan dalam variabel `SSH_HOST`.

#### Pipeline Schedule
![pipeline schedule](/images/2023-08-26_10-33-gitlab-pipeline-schedule.png "editor jadwal")
Dengan _scheduled pipeline_ kita dapat menjalankan _Gitlab CI/CD_ dengan interval terjadwal.[^2]
* *Descriptions*

  Deskripsi singkat dari _pipeline_.

* *Interval Patterns*

  Pola interval menggunakan [notasi kron](https://docs.gitlab.com/ee/topics/cron/index.html "notasi cron").

* *Cron timezone*

  Zona waktu yang akan digunakan oleh _pipeline_.

* *Select target branch or tag*

  _Branch_ atau _tags_ yang dipakai oleh _pipeline_.

* *Variables*

  _Variabel-variabel_ tambahan untuk _pipeline_.

![pipeline schedule](/images/2023-08-26_10-33-gitlab-pipeline-all-schedule.png "scheduled pipeline")

[^1]: https://docs.gitlab.com/runner
[^2]: https://docs.gitlab.com/ee/ci/pipelines/schedules.html
[^3]: https://docs.gitlab.com/ee/ci/variables/
