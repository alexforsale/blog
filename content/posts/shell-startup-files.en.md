---
title: "Shell Startup Files"
slug: "shell-startup-files"
description: "Setup Linux shell startup files"
date: 2023-09-03T17:02:37+07:00
tags: ['shell', 'script', 'bash', 'zsh', 'csh', 'tcsh', 'stow']
categories: ['shell']
draft: false
---

## Shell
From [wikipedia](https://en.wikipedia.org/wiki/Shell_(computing)):

> In computing, a shell is a computer program that exposes an operating system's services to a human user or other programs. In general, operating system shells use either a command-line interface (CLI) or graphical user interface (GUI), depending on a computer's role and particular operation. It is named a shell because it is the outermost layer around the operating system.
>
> Command-line shells require the user to be familiar with commands and their calling syntax, and to understand concepts about the shell-specific scripting language (for example, bash), while graphical shells place a low burden on beginning computer users and are characterized as being easy to use, yet most GUI-enabled operating systems also provide CLI shells, normally for performing advanced tasks.

## Shell Initialization Files
From [wikipedia](https://en.wikipedia.org/wiki/Unix_shell):

> Shells read configuration files in various circumstances. These files usually contain commands for the shell and are executed when loaded; they are usually used to set important variables used to find executables, like $PATH, and others that control the behavior and appearance of the shell. The table in this section shows the configuration files for popular shells.

| Configuration file        | sh                  | ksh         | csh   | tcsh    | bash                 | zsh         |
|:--------------------------|:-------------------:|:-----------:|:-----:|:-------:|:--------------------:|:-----------:|
| /etc/.login               |                     |             | login | login   |                      |             |
| /etc/csh.cshrc            |                     |             | yes   | yes     |                      |             |
| /etc/csh.login            |                     |             | login | login   |                      |             |
| ~/.tcshrc                 |                     |             |       | yes     |                      |             |
| ~/.cshrc                  |                     |             | yes   | yes[^a] |                      |             |
| ~/etc/ksh.kshrc           |                     | interactive |       |         |                      |             |
| /etc/sh.shrc              | interactive[^b]     |             |       |         |                      |             |
| $ENV (typically ~/.kshrc) | interactive[^c][^d] | interactive |       |         | interactive[^e]      |             |
| ~/.login                  |                     |             | login | login   |                      |             |
| ~/.logout                 |                     |             | login | login   |                      |             |
| /etc/profile              | login               | login       |       |         | login                | login[^f]   |
| ~/.profile                | login               | login       |       |         | login[^g]            | login[^f]   |
| ~/.bash_profile           |                     |             |       |         | login[^g]            |             |
| ~/.bash_login             |                     |             |       |         | login[^g]            |             |
| ~/.bash_logout            |                     |             |       |         | login                |             |
| ~/.bashrc                 |                     |             |       |         | interactive+nonlogin |             |
| /etc/zshenv               |                     |             |       |         |                      | yes         |
| /etc/zprofile             |                     |             |       |         |                      | login       |
| /etc/zshrc                |                     |             |       |         |                      | interactive |
| /etc/zlogin               |                     |             |       |         |                      | login       |
| /etc/zlogout              |                     |             |       |         |                      | login       |
| ~/.zshenv                 |                     |             |       |         |                      | yes         |
| ~/.zprofile               |                     |             |       |         |                      | login       |
| ~/.zshrc                  |                     |             |       |         |                      | interactive |
| ~/.zlogin                 |                     |             |       |         |                      | login       |

  * blank means a file is not read by a shell at all.
  * _yes_ means a file is always read by a shell upon startup.

#### login shell
A login shell is the shell where the user login. It is the first process that executes under the user ID, the login process tells the shell to behave as a login shell with a convention: passing argument 0, which is normally the name of the shell executable with a - prepended (e,g. `-bash`).

Using _Bash_, we can simply check the output of `$0`
```shell
echo $0
```
#### interactive shell
An interactive shell is one started without non-option arguments (unless -s is specified) and without specifying the -c option, whose input and error output are both connected to terminals (as determined by isatty(3)), or one started with the -i option.[^1]

An interactive shell generally reads from and writes to a user’s terminal.

The -s invocation option may be used to set the positional parameters when an interactive shell is started.

#### non-interactive shell
A non-interactive shell is a type of shell that doesn’t interact with the user. We can run it through a script or similar. Also, it can be run through some automated process.

In _Bash_, we can check it with the command:
```shell
[[ $- == *i* ]] && echo ‘Interactive’ || echo ‘not-interactive’
```

### My shell configuration files

Just like my other configuration files, I keep my shell initialization files version-controlled in [gitlab](https://gitlab.com/alexforsale/dotfiles-shells "dotfiles-shells"). This repository is meant to be read by many _posix-compliance_ shells and deployed into my home directory using [GNU Stow](https://www.gnu.org/software/stow/ "GNU Stow") as a managed symbolic link,

```shell
alexforsale@somalia-minimal ~/.dotfiles/shells $ tree . -la
.
├── .config
│ └── profile.d
│     ├── 00-distro.sh
│     ├── 00-locale.sh
│     ├── 01-xdg_base_directory.sh
│     ├── 02-editors.sh
│     ├── 03-terminals.sh
│     ├── 04-security.sh
│     ├── 05-filemanagers.sh
│     ├── 06-browser.sh
│     ├── 07-mail_apps.sh
│     ├── 10-polybar.sh
│     ├── 10-themes.sh
│     ├── 99-cargo.sh
│     ├── 99-ccache.sh
│     ├── 99-composer.sh
│     ├── 99-dash.sh
│     ├── 99-doom_emacs.sh
│     ├── 99-elinks.sh
│     ├── 99-emacs_vterm.sh
│     ├── 99-freebsd.sh
│     ├── 99-fsharp.sh
│     ├── 99-ghcup.sh
│     ├── 99-go.sh
│     ├── 99-guix.sh
│     ├── 99-nano.sh
│     ├── 99-nix.sh
│     ├── 99-npm.sh
│     ├── 99-password-store.sh
│     ├── 99-perl.sh
│     ├── 99-python.sh
│     ├── 99-ruby.sh
│     └── 99-screen.sh
├ .git
├ .login
├ .logout
├ .profile
├ .shrc
└── README.org

3 directories, 37 files
```

As seen from the directory structure, I keep everything modularized, this keeps each file at minimum.

#### ~/.shrc
I usually keep the default from _FreeBSD_ for this file with a few small additions.
```shell
#!/bin/sh
# $FreeBSD$
#
# .shrc - bourne shell startup file
#
# This file will be used if the shell is invoked for interactive use and
# the environment variable ENV is set to this file.
#
# see also sh(1), environ(7).
#

# file permissions: rwxr-xr-x
#
# umask	022

# Uncomment this to enable the builtin vi(1) command line editor in sh(1),
# e.g. ESC to go into visual mode.
# set -o vi

if ([ "${SHELL}" != "/bin/sh" ] && [ "${SHELL}" != "/bin/dash" ]);then
   return
fi

alias h='fc -l'
alias j=jobs
alias m="$PAGER"
alias ll='ls -laFo'
alias l='ls -l'
alias g='egrep -i'

# # be paranoid
# alias cp='cp -ip'
# alias mv='mv -i'
# alias rm='rm -i'

# set prompt: ``username@hostname:directory $ ''
if [ "${DISTRO}" = "freebsd" ]; then
    PS1="\u@\h:\w \\$ "
else
    PS1="${USER}@${HOSTNAME}:\${PWD} $ "
fi

# search path for cd(1)
# CDPATH=:$HOME
```

The first addition the I made is:
```shell
if ([ "${SHELL}" != "/bin/sh" ] && [ "${SHELL}" != "/bin/dash" ]);then
   return
fi
```
This ensure that this file won't be sourced if the running shell is not "/bin/sh" or "/bin/dash", since this is only compatible for those two.

The second one is:
```shell
if [ "${DISTRO}" = "freebsd" ]; then
    PS1="\u@\h:\w \\$ "
else
    PS1="${USER}@${HOSTNAME}:\${PWD} $ "
fi
```

This will set the _PS1_ environment variable according to the _$DISTRO_ variable.

#### ~/.login
```shell
#!/usr/bin/env tcsh
# -*- mode: sh -*-
# $FreeBSD$
#
# .login - csh login script, read by login shell, after `.cshrc' at login.
#
# See also csh(1), environ(7).
#

# Query terminal size; useful for serial lines.
if ( -x /usr/bin/resizewin ) /usr/bin/resizewin -z
```

This file is read by _tcsh_ and _csh_, and it's the default from _FreeBSD_. The command `resizewin` is a utility to query and set the terminal size (see the [manpage](https://man.freebsd.org/cgi/man.cgi?resizewin(1)) for full explanation)

#### ~/.logout
```shell
#!/usr/bin/env tcsh
# -*- mode: sh -*-
#
# .logout - csh logout script
#

if ( -x `which clear` ) clear
```

When using _tcsh_ or _csh_, this file will ensure the terminal is cleared whenever the user is logged out from the terminal.

#### ~/.profile
This one is the important part. So I will explain it in portions.

This will check and source  _/etc/profile_ if it exists.
```shell
#!/bin/sh
# ~/.profile
# Environment and startup programs.
# source /etc/profile if exist.
# <alexforsale@yahoo.com>
#

[ -f /etc/profile ] && . /etc/profile
```

If theres a _$HOME/bin_ and/or _$HOME/.local/bin_ directory, include it in the _$PATH_ variable.
```shell
# this goes first in case others needs it.
if [ -d "${HOME}/bin" ] ; then
    export PATH="${HOME}/bin:${PATH}"
fi
if [ -d "$HOME/.local/bin" ];then
   export PATH="${HOME}/.local/bin:${PATH}"
fi
```
This will load all _.sh_ files in the _$HOME/.config/profile.d_ directory. The shell will source the file based on their filenames, so usually I use a _[number]-[filename].sh_ filename convention to set the order for the files.
```shell
# Loads user profiles if exists. Should be in ~/.profile.d
# but let's not pollute ~ anymore.

if [ -d "${HOME}/.config/profile.d" ]; then
    for profile in "${HOME}"/.config/profile.d/*.sh; do
        . "${profile}"
    done
    unset profile
fi
```
I forgot where I got this, but this is a cool code that will clear duplicates from the _$PATH_ variable.
```shell
if [ -n "${PATH}" ]; then
    old_PATH=${PATH}:; PATH=
    while [ -n "${old_PATH}" ]; do
        x=${old_PATH%%:*}       # the first remaining entry
        case ${PATH}: in
            *:"$x":*) ;;         # already there
            *) PATH=${PATH}:$x;;    # not there yet
        esac
        old_PATH=${old_PATH#*:}
    done
    PATH=${PATH#:}
    unset old_PATH x
fi
```
This one will source _$HOME/.config/profile.local_ or _$HOME/.profile.local_ if exists. This is intentionally set last so it can override any other variables set earlier.
```shell
# local ~/.profile
if [ -r "${HOME}"/.config/profile.local ];then
   . "${HOME}"/.config/profile.local
   elif [ -r "${HOME}"/.profile.local ];then
        . "${HOME}"/.profile.local
   fi
```
Specifically for _Bash_, this will source _$HOME/.bashrc_ in case _$HOME/.bash_profile_ or _$HOME/.bash_login_ is not exists or don't source _$HOME/.bashrc_.
```shell
# if running bash
if [ -n "${BASH_VERSION}" ]; then
    # include .bashrc if it exists
    if [ -f "${HOME}/.bashrc" ]; then
        . "${HOME}/.bashrc"
    fi
fi
```

#### The rest of the modules
I won't display them all since most of them are for specific apps/programs (and most of them are from their respective wiki/documentation). But some noteworthy ones:

##### ~/.config/profile.d/00-distro.sh
This file defines the variable _DISTRO_ and _DISTROVER_, based on the operating system.
```shell
if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    DISTRO="${ID}"
    DISTROVER="${VERSION_ID}"
    [ -z "${DISTROVER}" ] &&
        DISTROVER="${BUILD_ID}"
elif [ "$(command -v lsb_release >/dev/null)" ]; then
    # linuxbase.org
    DISTRO="$(lsb_release -si | awk '{print tolower ($0)}')"
    DISTROVER="$(lsb_release -sr | awk '{print tolower ($0)}')"
elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    DISTRO="${DISTRIB_ID}"
    DISTROVER="${DISTRIB_RELEASE}"
elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    DISTRO=Debian
    DISTROVER="$(cat /etc/debian_version)"
else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    DISTRO="$(uname -s)"
    DISTROVER="$(uname -r)"
fi
```

[^a]: only if ~/.tcshrc not found
[^b]: newer version of Bourne Shells only
[^c]: Available on systems that support the "User Portability Utilities option"
[^d]: $ENV is $HOME/.shrc in newer versions of the Bourne Shell
[^e]: Same behavior as sh, but only if invoked as sh (bash 2+) or, since bash 4.2, also if invoked explicitly in POSIX compatibility mode (with options --posix or -o posix).
[^f]: Only in sh/ksh compatibility mode (when invoked as bash, sh, ksh)
[^g]: The first readable file in order of ~/.bash_profile, ~/.bash_login and ~/.profile; and only ~/.profile if invoked as sh or, as of at least Bash 4.2, if invoked explicitly in POSIX compatibility mode (with options --posix or -o posix)
[^1]: https://www.gnu.org/software/bash/manual/html_node/What-is-an-Interactive-Shell_003f.html#What-is-an-Interactive-Shell_003f
